/*
1)Опишіть своїми словами що таке Document Object Model (DOM)
2)Яка різниця між властивостями HTML-елементів innerHTML та innerText?
3)Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?


1) DOM - об'єктна модель документів, за допомогою якої JavaScript отримує доступ
до контенту сторінки(тегів, коментарів, тощо)
2) innerHTML - показує весь вміст з HTML-тегами, а innerText - показує тільки текст,
ігноруючи HTML-теги
3) звернутись можна за допомогую методів: 
     document.getElementById()
     document.getElementsByName() 
     document.getElementsByTagName()
     document.getElementsByClassName() 

     але найбільш вживанні:
     document.querySelector() 
     document.querySelectorAll()
*/


//Знайти всі параграфи на сторінці та встановити колір фону #ff0000
const allParagraph = document.querySelectorAll('p')
allParagraph.forEach(element => {
  element.style.backgroundColor = "#ff0000"
})


// Знайти елемент із id="optionsList". Вивести у консоль.
const optionsList = document.querySelector("#optionsList")
//const optionsList = document.getElementById("optionsList")
console.log(optionsList)
// Знайти батьківський елемент та вивести в консоль. 
const parent = optionsList.parentElement
console.log(parent)

// Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.
if (parent.hasChildNodes()) {
  for (const element of parent.childNodes) {
    console.log(`name element - ${element.nodeName}, type element - ${element.nodeType}`);
  }
}

//Встановіть в якості контента елемента з класом testParagraph
// наступний параграф -This is a paragraph
const testParagraph = document.querySelector(".testParagraph")
if (testParagraph) {
  console.log(testParagraph)
} else {
  console.log(`element with class 'testParagraph' not find`);
}

//Отримати елементи, вкладені в елемент із класом main-header 
//і вивести їх у консоль.
const mainHeaderElements = document.querySelector(".main-header")
console.log(mainHeaderElements.children)

// Кожному з елементів присвоїти новий клас nav-item.
const newClassNavItem = Array.from(mainHeaderElements.children)
newClassNavItem.forEach(element => {
  element.classList.add("nav-item")
});
console.log(mainHeaderElements.children)


//Знайти всі елементи із класом section-title. 
const elementSectionTitle = document.querySelectorAll(".section-title")
console.log(elementSectionTitle)
//Видалити цей клас у цих елементів.
Array.from(elementSectionTitle).forEach(element => {
  element.classList.remove("section-title")
})
console.log(elementSectionTitle)